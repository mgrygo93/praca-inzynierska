#include "custom_SIR.h"
#include <time.h>
#include "graph.h"
#include "functions.h"

custom_SIR::custom_SIR() {
}

custom_SIR::custom_SIR(graph &ExistingGraph, float InfectRate, float RemovingRate) {
	int VerticeNumber = ExistingGraph.getVerticeNumber();
	Infectiousness = InfectRate;
	//RecoveryRate = 0;
	RemoveRate = RemovingRate;

	for (int i = 0; i < ExistingGraph.getObserversNumber(); i++) {
		ObserversStatusChanged.push_back(99999);
	}

	InfectedNumber = 0;
	SusceptibleNumber = VerticeNumber;
	RemovedNumber = 0;

	//creating an array holding epidemy state at single timestep
	EpidemyState = new int[VerticeNumber];
	for (int i = 0; i < VerticeNumber; i++)	{
		EpidemyState[i] = 0;
	}

	Graph = &ExistingGraph;
}

custom_SIR::~custom_SIR() {
	delete[] EpidemyState;
}

void custom_SIR::getEpidemyStateInFile(FILE &file) {
	fprintf(&file, "%d %d %d\n", SusceptibleNumber, InfectedNumber, RemovedNumber);
}

void custom_SIR::resetEpidemyStates() {
	for (int i = 0; i < Graph->getVerticeNumber(); i++)
		EpidemyState[i] = 0;

	InfectedNumber = 0;
	SusceptibleNumber = Graph->getVerticeNumber();
	RemovedNumber = 0;
}

int custom_SIR::getRemovedNumber() {
	return RemovedNumber;
}

/*Susceptible = Ignorant, Infected = Spreader, Removed/Recoverd = Stifler
"[...] so that the contacting spreaders become stiflers with probability 'alpha' if they encounter another spreader or a stifler."
*/
void custom_SIR::simulateEpidemy(int TimeStepsNumber, int UpdatesNumber, std::string fileName, bool breakIfCuredQuickly, bool &simulationSucceeded) {
	remove(fileName.c_str());
	SusceptibleNumber = Graph->getVerticeNumber() - InfectedNumber;

	FILE *EpidemyFile = fopen(fileName.c_str(), "w");

	fprintf(EpidemyFile, "S I R\n");
	getEpidemyStateInFile(*EpidemyFile);

	int VerticeNumber = Graph->getVerticeNumber();
	std::vector <int> Observers = Graph->getObservers();

	int i = 0;
	for (i = 0; i < TimeStepsNumber; i++) {
		for (int j = 0; j < UpdatesNumber; j++) {
			//SPREAD RUMORS IF SPREADER			
			int InfectingIndex = RandomInteger(VerticeNumber - 1);
			if (Graph->Structure[InfectingIndex].size() > 0 && EpidemyState[InfectingIndex] == 1) {
				for (unsigned int k = 0; k < Graph->Structure[InfectingIndex].size(); k++) {
					//firstly we check if neighbouring vertex is an ignorant
					if (EpidemyState[Graph->Structure[InfectingIndex][k]] == 0) {
						float InfectionTest = (float)rand() / RAND_MAX;
						if (InfectionTest > (1 - Infectiousness)) {
							EpidemyState[Graph->Structure[InfectingIndex][k]] = 1;
							InfectedNumber++;
							SusceptibleNumber--;

							if (std::find(Observers.begin(), Observers.end(), Graph->Structure[InfectingIndex][k]) != Observers.end()) {
								int pos = std::find(Observers.begin(), Observers.end(), Graph->Structure[InfectingIndex][k]) - Observers.begin();
								ObserversStatusChanged[pos] = i;
							}
						}
					} 
					//other options than ignorant are another spreader or stifler, so 'else' covers all of them
					else {
						float RemovingTest = (float)rand() / RAND_MAX;
						//if spreader meets another spreader or stifler it might become stifler and stops further rumour spreading
						if (RemovingTest > (1 - RemoveRate) && EpidemyState[InfectingIndex] != 2) {
							EpidemyState[InfectingIndex] = 2;
							InfectedNumber--;
							RemovedNumber++;
						}
					}
				}
			}
		}
		getEpidemyStateInFile(*EpidemyFile);

		if (breakIfCuredQuickly) {
			if (i < 10 && InfectedNumber == 0) {
				simulationSucceeded = false;
				break;
			}
		}
	}

	fclose(EpidemyFile);

	if (i == TimeStepsNumber) {
		simulationSucceeded = true;
	}
}

void custom_SIR::simulate_epidemy_no_file(int TimeStepsNumber, int UpdatesNumber, bool breakIfCuredQuickly, bool &simulationSucceeded) {
	SusceptibleNumber = Graph->getVerticeNumber() - InfectedNumber;

	int VerticeNumber = Graph->getVerticeNumber();
	std::vector <int> Observers = Graph->getObservers();

	int i = 0;
	for (i = 0; i < TimeStepsNumber; i++) {
		for (int j = 0; j < UpdatesNumber; j++) {
			//SPREAD RUMORS IF SPREADER			
			int InfectingIndex = RandomInteger(VerticeNumber - 1);
			if (Graph->Structure[InfectingIndex].size() > 0 && EpidemyState[InfectingIndex] == 1) {
				for (unsigned int k = 0; k < Graph->Structure[InfectingIndex].size(); k++) {
					//firstly we check if neighbouring vertex is an ignorant
					if (EpidemyState[Graph->Structure[InfectingIndex][k]] == 0) {
						float InfectionTest = (float)rand() / RAND_MAX;
						if (InfectionTest >(1 - Infectiousness)) {
							EpidemyState[Graph->Structure[InfectingIndex][k]] = 1;
							InfectedNumber++;
							SusceptibleNumber--;

							if (std::find(Observers.begin(), Observers.end(), Graph->Structure[InfectingIndex][k]) != Observers.end()) {
								int pos = std::find(Observers.begin(), Observers.end(), Graph->Structure[InfectingIndex][k]) - Observers.begin();
								ObserversStatusChanged[pos] = i;
							}
						}
					}
					//other options than ignorant are another spreader or stifler, so 'else' covers all of them
					else {
						float RemovingTest = (float)rand() / RAND_MAX;
						//if spreader meets another spreader or stifler it might become stifler and stops further rumour spreading
						if (RemovingTest > (1 - RemoveRate) && EpidemyState[InfectingIndex] != 2) {
							EpidemyState[InfectingIndex] = 2;
							InfectedNumber--;
							RemovedNumber++;
						}
					}
				}
			}
		}

		if (breakIfCuredQuickly) {
			if (i < 10 && InfectedNumber == 0) {
				simulationSucceeded = false;
				break;
			}
		}
	}

	if (i == TimeStepsNumber) {
		simulationSucceeded = true;
	}
}