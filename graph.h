#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include <array>

class graph
{
protected:
	int VerticeNumber;
	float VertexDegree;	
public:
	std::vector<int>* Structure;
	std::vector < std::array<int, 2> > Edges;
	std::vector <int> Observers;

	graph();
	~graph();
	
	int getChosenIndexDegree(int index);
	void inFileStructure(std::string filename);
	void inFileEdges(std::string filename);
	void setVerticeNumber(int Number);
	int getVerticeNumber();
	void setVertexDegree(float Degree);
	float getVertexDegree();
	void getEdgesFromFile(std::string filename);
	void createStructureFromEdges();
	void addObserver(int observerIndex);
	void addObservers(std::vector <int> observersIndices);
	void addObservers_randomly(int number_of_observers);
	void clearObservers();
	std::vector <int> getObservers();
	int getObserversNumber();
	virtual void createGraphStructure(bool writeOperationTime) = 0;
	virtual void resetGraphStructure() = 0;
};

#endif //GRAPH