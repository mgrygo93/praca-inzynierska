#include "EpidemyModel.h"
#include "graph.h"

EpidemyModel::EpidemyModel() {
}

EpidemyModel::~EpidemyModel() {
	delete[] EpidemyState;
}

int EpidemyModel::getVerticeNumber() {
	return Graph->getVerticeNumber();
}

int EpidemyModel::getChosenIndexDegree(int index) {
	return Graph->getChosenIndexDegree(index);
}

std::string EpidemyModel::getObserversStatusChanged() {
	std::string status;
	for (int i = 0; i < Graph->getObserversNumber(); i++) {
		if (i == 0) {
			status = std::to_string(ObserversStatusChanged[i]);
		} else {
			status += "," + std::to_string(ObserversStatusChanged[i]);
		}
	}
	return status;
}

std::string EpidemyModel::getObserversStatusChanged_desiredFormat() {
	std::string status;
	int relative_value;
	bool has_not_reached = true;

	for (int i = 0; i < Graph->getObserversNumber(); i++) {
		if (i == 0) {
			while (has_not_reached) {
				if (ObserversStatusChanged[i] != 99999 || i == (Graph->getObserversNumber()-1)) {
					relative_value = ObserversStatusChanged[i];
					has_not_reached = false;					
				}
				i++;
			}
			i = 0;
			status = std::to_string(ObserversStatusChanged[i] - relative_value);
		}
		else {
			if (ObserversStatusChanged[i] != 99999) {
				status += "," + std::to_string(ObserversStatusChanged[i] - relative_value);
			} else {
				status += "," + std::to_string(ObserversStatusChanged[i]);
			}
		}
	}
	return status;
}

float EpidemyModel::getInfectiousness() {
	return Infectiousness;
}

void EpidemyModel::setInfectiousness(float infectiousness) {
	Infectiousness = infectiousness;
}

float EpidemyModel::getRecoveryRate() {
	return RecoveryRate;
}

void EpidemyModel::setRecoveryRate(float recoveryRate) {
	RecoveryRate = recoveryRate;
}

void EpidemyModel::setGraph(graph &graph) {
	Graph = &graph;
}

int EpidemyModel::getInfectedNumber() {
	return InfectedNumber;
}

int EpidemyModel::getSusceptibleNumber() {
	return SusceptibleNumber;
}

int EpidemyModel::getRemovedNumber() {
	return RemovedNumber;
}

void EpidemyModel::saveInFileGraph(char* filename) {
	Graph->inFileStructure(filename);
}

void EpidemyModel::saveEpidemyState(FILE &file) {
	for (int i = 0; i < Graph->getVerticeNumber(); i++)
		fprintf(&file, "%d ", EpidemyState[i]);

	fprintf(&file, "\n");
}

void EpidemyModel::infectWithIndex(int index) {
	EpidemyState[index] = 1;
}

void EpidemyModel::infectFirstWithNeighbours() {
	bool InfectionHappened = false;
	int i = 0;
	while (!InfectionHappened) {
		if (Graph->Structure[i].size() > 0) {
			EpidemyState[i] = 1;
			InfectedNumber++;
			InfectionHappened = true;
		}
		i++;
	}
}

//Which means which vertex with chosen number of neighbours is going to be infected i.e. first, third, 201st
//Which should start from 1
bool EpidemyModel::infectWithNumberOfNeighbours(int NumberOfNeighbours, int Which) {
	bool InfectionHappened = false;
	int j = 0;
	int i = 0;
	while (!InfectionHappened && i < Graph->getVerticeNumber()) {
		if (Graph->Structure[i].size() == NumberOfNeighbours) {
			j++;
			if (j == Which) {
				if (EpidemyState[i] == 0) {
					EpidemyState[i] = 1;
					InfectedNumber++;
				}
				InfectionHappened = true;
				printf("Infected %d. vertex!\n", i + 1);
			}
		}
		i++;
	}

	if (!InfectionHappened) {
		printf("No suitable vertex to infect!\n");
	}

	return InfectionHappened;
}

void EpidemyModel::resetGraphStructure() {
	Graph->resetGraphStructure();
}