#ifndef EPIDEMYMODEL_H
#define EPIDEMYMODEL_H

#include "graph.h"
#include "vector"
#include "string"

class EpidemyModel
{
protected:
	graph* Graph;
	int* EpidemyState;
	//Infectiousness and RecoveryRate should be from <0,1>
	float Infectiousness;
	float RecoveryRate;	

	int InfectedNumber;
	int SusceptibleNumber;
	int RemovedNumber;
	//Stores number of timestep at which given observer becomes infected
	std::vector <int> ObserversStatusChanged;
public:
	EpidemyModel();
	~EpidemyModel();

	int getVerticeNumber();
	int getChosenIndexDegree(int index);
	std::string getObserversStatusChanged();
	std::string EpidemyModel::getObserversStatusChanged_desiredFormat();

	float getInfectiousness();
	void setInfectiousness(float infectiousness);
	float getRecoveryRate();
	void setRecoveryRate(float recoveryRate);
	void setGraph(graph &graph);

	int getInfectedNumber();
	int getSusceptibleNumber();
	int getRemovedNumber();

	void saveInFileGraph(char* filename);
	void saveEpidemyState(FILE &file);
	virtual void resetEpidemyStates() = 0;

	void infectWithIndex(int index);
	void infectFirstWithNeighbours();
	bool infectWithNumberOfNeighbours(int NumberOfNeighbours, int Which);
	virtual void getEpidemyStateInFile(FILE &file) = 0;

	virtual void simulateEpidemy(int TimeStepsNumber, int UpdatesNumber, std::string fileName, bool breakIfCuredQuickly, bool &simulationSucceeded) = 0;
	virtual void simulate_epidemy_no_file(int TimeStepsNumber, int UpdatesNumber, bool breakIfCuredQuickly, bool &simulationSucceeded) = 0;
	void resetGraphStructure();
};

#endif //EPIDEMYMODEL