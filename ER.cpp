#include <array>
#include <vector>
#include <algorithm>
#include <time.h>
#include "functions.h"
#include "ER.h"

ER::ER() {
}

ER::ER(int Number, float Degree) {
	VerticeNumber = Number;
	VertexDegree = Degree;

	createGraphStructure(false);
}

ER::ER(int Number, std::string filename) {
	VerticeNumber = Number;
	VertexDegree = 0; //Unknown

	clock_t t1, t2;
	t1 = clock();

	getEdgesFromFile(filename);
	Structure = new std::vector<int>[VerticeNumber];
	createStructureFromEdges();

	t2 = clock();

	float OperationsTime = (float)(t2 - t1) / 1000;
	printf("Loading ER graph from file took: %f seconds!\n", OperationsTime);
}


ER::~ER() {
	delete[] Structure;
}

//requires Edges to be sorted
void ER::removeNeighbouringDoubles(std::vector < std::array<int, 2> > &Edges) {
	for (unsigned int i = 1; i < Edges.size(); i++) {
		if (Edges[i - 1][0] == Edges[i][0] && Edges[i - 1][1] == Edges[i][1]) {			
			std::vector <int> VerticeNeighbours;
			int VerticeIndex = Edges[i - 1][0];
			int j = i;
			while (Edges[j - 1][0] == VerticeIndex) {
				j++;
				VerticeNeighbours.push_back(Edges[j - 1][1]);
			}

			while (std::find(VerticeNeighbours.begin(), VerticeNeighbours.end(), Edges[i - 1][1]) != VerticeNeighbours.end() || Edges[i - 1][1] == Edges[i - 1][0]) {
				Edges[i - 1][1] = RandomInteger(VerticeNumber - 1);
			}
		}
	}
}

bool ER::edgesEquals(std::array<int, 2> firstEdge, std::array<int, 2> secondEdge) {
	bool edgesEquals = false;

	if ((firstEdge[0] == secondEdge[0] && firstEdge[1] == secondEdge[1]) || (firstEdge[0] == secondEdge[1] && firstEdge[1] == secondEdge[0]))
		edgesEquals = true;

	return edgesEquals;
}

int ER::getDoubledEdgesNumber(std::vector < std::array<int, 2> > Edges) {
	int repeat = 0;

	for (unsigned int i = 1; i < Edges.size(); i++) {
		for (unsigned int j = i; j < Edges.size(); j++) {
			if (edgesEquals(Edges[i-1], Edges[j]))
				repeat++;
		}
	}
	return repeat;
}

void ER::createGraphStructure(bool writeOperationTime) {
	clock_t t1, t2;
	if (writeOperationTime) {
		t1 = clock();
	}

	//creating a vector of graph's edges
	int EdgesNumber = VertexDegree*VerticeNumber / 2;
	Edges.reserve(EdgesNumber);

	int i = 0;
	while (i < EdgesNumber) {
		int FirstIndex = RandomInteger(VerticeNumber - 1);
		int SecondIndex = RandomInteger(VerticeNumber - 1);
		while (FirstIndex == SecondIndex)
			SecondIndex = RandomInteger(VerticeNumber - 1);

		std::array<int, 2> Edge = { FirstIndex, SecondIndex };
		Edges.push_back(Edge);
		i++;
	}

	/*
	//usuwanie powtorzen - uwaga jak mamy 5,21 5,21 5,23, to sie pozbedziemy powtorzenia, ale co jak mamy 5,234 [...] 234,5? nierozwiazane!(*)
	remove("Edges.txt");
	FILE *file = fopen("Edges.txt", "w");
	std::sort(Edges.begin(), Edges.end());
	//int mirroredRepeat = 0; //(*)

	fprintf(file, "There's %d repetitons before fix\n", getDoubledEdgesNumber(Edges));

	removeNeighbouringDoubles(Edges);

	fprintf(file, "\nThere's %d repetitons after fix", getDoubledEdgesNumber(Edges));
	fclose(file);
	//koniec usuwania powtorzen
	*/

	//applies edges to graph
	Structure = new std::vector<int>[VerticeNumber];
	for (int i = 0; i < EdgesNumber; ++i) {
		int EdgeFirstIndex = Edges[i][0];
		int EdgeSecondIndex = Edges[i][1];
		Structure[EdgeSecondIndex].push_back(EdgeFirstIndex);
		Structure[EdgeFirstIndex].push_back(EdgeSecondIndex);
	}

	if (writeOperationTime) {
		t2 = clock();
		float OperationsTime = (float)(t2 - t1) / 1000;
		printf("Creating ER graph took: %f seconds!\n", OperationsTime);
	}
}

void ER::resetGraphStructure() {
	delete[] Structure;
	Edges.clear();
	createGraphStructure(false);
}