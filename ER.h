#ifndef ER_H //Erdos-Renyi graph
#define ER_H

#include "graph.h"
#include <array>
#include <vector>
#include <string>

class ER : public graph
{
public:
	ER();
	ER(int Number, float Degree);
	ER(int Number, std::string filename);
	~ER();
	
	void removeNeighbouringDoubles(std::vector < std::array<int, 2> > &Edges);
	bool edgesEquals(std::array<int, 2> firstEdge, std::array<int, 2> secondEdge);
	int getDoubledEdgesNumber(std::vector < std::array<int, 2> > Edges);

	virtual void createGraphStructure(bool writeOperationTime);
	virtual void resetGraphStructure();
};

#endif //ER
