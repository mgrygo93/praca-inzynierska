#ifndef SIR_H
#define SIR_H

#include "EpidemyModel.h"
#include "graph.h"

class SIR : public EpidemyModel
{
private:
	float RemoveRate;
public:
	SIR();
	SIR(graph &ExistingGraph, float InfectRate, float RemovingRate);
	~SIR();

	virtual void getEpidemyStateInFile(FILE &file);

	virtual void resetEpidemyStates();	
	virtual void simulateEpidemy(int TimeStepsNumber, int UpdatesNumber, std::string fileName, bool breakIfCuredQuickly, bool &simulationSucceeded);
	virtual void simulate_epidemy_no_file(int TimeStepsNumber, int UpdatesNumber, bool breakIfCuredQuickly, bool &simulationSucceeded);
};

#endif //SIR
