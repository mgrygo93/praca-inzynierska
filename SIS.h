#ifndef SIS_H
#define SIS_H

#include "EpidemyModel.h"
#include "graph.h"

class SIS : public EpidemyModel
{
public:
	SIS();
	SIS(graph &ExistingGraph, float InfectRate, float RecoverRate);
	~SIS();

	virtual void getEpidemyStateInFile(FILE &file);

	virtual void resetEpidemyStates();
	virtual void simulateEpidemy(int TimeStepsNumber, int UpdatesNumber, std::string fileName, bool breakIfCuredQuickly, bool &simulationSucceeded);
	virtual void simulate_epidemy_no_file(int TimeStepsNumber, int UpdatesNumber, bool breakIfCuredQuickly, bool &simulationSucceeded);
};

#endif //SIS