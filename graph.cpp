#include "graph.h"
#include <time.h>
#include <string>
#include <fstream>
#include <array>
#include <vector>
#include "functions.h"

graph::graph() {
}

graph::~graph() {
}

int graph::getChosenIndexDegree(int index) {
	return Structure[index].size();
}

void graph::inFileStructure(std::string filename) {
	remove(filename.c_str());
	FILE *file = fopen(filename.c_str(), "w");

	for (int i = 0; i < VerticeNumber; i++) {
		for (unsigned int j = 0; j < Structure[i].size(); j++)
			fprintf(file, "%d ", Structure[i][j]);
		fprintf(file, "\n");
	}

	fclose(file);
}

void graph::inFileEdges(std::string filename) {
	remove(filename.c_str());
	FILE *file = fopen(filename.c_str(), "w");

	fprintf(file, "Source Target\n");
	for (unsigned int i = 0; i < Edges.size(); i++) {
		fprintf(file, "%d %d\n", Edges[i][0], Edges[i][1]);
	}

	fclose(file);
}

void graph::setVerticeNumber(int Number) {
	VerticeNumber = Number;
}

int graph::getVerticeNumber() {
	return VerticeNumber;
}

void graph::setVertexDegree(float Degree) {
	VertexDegree = Degree;
}

float graph::getVertexDegree() {
	return VertexDegree;
}

void graph::getEdgesFromFile(std::string filename) {
	std::fstream myfile(filename, std::ios_base::in);

	//skipping first line "Source Target"
	std::string dummyLine;
	getline(myfile, dummyLine);

	float a;
	int i = 0;
	std::array<int, 2> edge;
	while (myfile >> a)
	{
		if (i % 2 == 0) {
			edge[0] = a;
		} else {
			edge[1] = a;
			Edges.push_back(edge);
		}
		i++;
	}

	myfile.close();
}

void graph::createStructureFromEdges() {
	for (unsigned int i = 0; i < Edges.size(); ++i) {
		int EdgeFirstIndex = Edges[i][0];
		int EdgeSecondIndex = Edges[i][1];
		Structure[EdgeSecondIndex].push_back(EdgeFirstIndex);
		Structure[EdgeFirstIndex].push_back(EdgeSecondIndex);
	}
}

void graph::addObserver(int observerIndex) {
	Observers.push_back(observerIndex);
}

void graph::addObservers(std::vector <int> observersIndices) {
	Observers = observersIndices;
}

void graph::addObservers_randomly(int number_of_observers) {
	int observer_index;
	bool has_neighbours = false;
	for (int i = 0; i < number_of_observers; i++) {
		while (!has_neighbours) {
			observer_index = RandomInteger(VerticeNumber - 1);
			if (getChosenIndexDegree(observer_index) > 0) {
				has_neighbours = true;
			}
		}
		Observers.push_back(observer_index);
		has_neighbours = false;
	}
}

void graph::clearObservers() {
	Observers.clear();
}

std::vector <int> graph::getObservers() {
	return Observers;
}

int graph::getObserversNumber() {
	return Observers.size();
}