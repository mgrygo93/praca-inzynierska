#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "EpidemyModel.h"
#include <vector>
#include <string>

int RandomInteger(int MaxValue);
float RandomFloat();
bool Equals(int* Array1, int* Array2, int ElementsNumber);
void performSingleSimulation(EpidemyModel* epidemyModel, int numberOfNeighbours, int which, std::string fileName, int timeStepNumber, int updatesNumber);
//only for SIR and custom_SIR
void performSimulationForRemoved(EpidemyModel* epidemyModel, int index, std::string fileName, int timeStepNumber, int updatesNumber);
void performSimulationForRemoved_no_file(EpidemyModel* epidemyModel, int index, int timeStepNumber, int updatesNumber);
//pobieranie R od indexu i stopnia wierzcholka
void performSimulationForEveryNodeForRemoved(std::string fileName, EpidemyModel* epidemyModel, int timeStepNumber, int updatesNumber);
void saveDataForMutualInformation(std::string filename, std::vector<std::string> v1, std::vector<int> v2);
void write_estimated_time_left(int current_timestep_number, int number_of_iterations, float operations_time_so_far);
int get_number_of_chars_in_line(std::string filename);

void calculate_mutual_information_in_file(EpidemyModel* epidemyModel, std::string filename, int number_of_iterations, int number_of_data_points, bool write_status, std::string histogram_filename);
void calculate_mutual_information_in_file_same_topology_for_models(EpidemyModel* epidemyModel_1, std::string filename_1,
	EpidemyModel* epidemyModel_2, std::string filename_2, int number_of_iterations, int number_of_data_points, bool write_status);
void MI_ER_BA(EpidemyModel* epidemyModel_1, EpidemyModel* epidemyModel_2, int MI_datapoints_number, std::string epidemy_model, int number_of_iterations);

#endif //FUNTCIONS_H