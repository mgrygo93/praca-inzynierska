#include <time.h>
#include "SI.h"
#include "graph.h"
#include "functions.h"
#include <algorithm>
#include <vector>
#include <iostream>

SI::SI() {
}

SI::SI(graph &ExistingGraph, float InfectRate) {
	int VerticeNumber = ExistingGraph.getVerticeNumber();
	Infectiousness = InfectRate;
	//RecoveryRate = 0;

	for (int i = 0; i < ExistingGraph.getObserversNumber(); i++) {
		ObserversStatusChanged.push_back(99999);
	}

	InfectedNumber = 0;
	SusceptibleNumber = VerticeNumber;
	RemovedNumber = 0;

	//creating an array holding epidemy state at single timestep
	EpidemyState = new int[VerticeNumber];
	for (int i = 0; i < VerticeNumber; i++)	{
		EpidemyState[i] = 0;
	}

	Graph = &ExistingGraph;
}

SI::~SI() {
	delete[] EpidemyState;
}

void SI::getEpidemyStateInFile(FILE &file) {
	fprintf(&file, "%d %d\n", SusceptibleNumber, InfectedNumber);
}

void SI::resetEpidemyStates() {
	for (int i = 0; i < Graph->getVerticeNumber(); i++)
		EpidemyState[i] = 0;

	InfectedNumber = 0;
	SusceptibleNumber = Graph->getVerticeNumber();
}

void SI::simulateEpidemy(int TimeStepsNumber, int UpdatesNumber, std::string fileName, bool breakIfCuredQuickly, bool &simulationSucceeded) {
	remove(fileName.c_str());
	SusceptibleNumber = Graph->getVerticeNumber() - InfectedNumber;

	FILE *EpidemyFile = fopen(fileName.c_str(), "w");

	fprintf(EpidemyFile, "S I\n");
	getEpidemyStateInFile(*EpidemyFile);

	int VerticeNumber = Graph->getVerticeNumber();
	std::vector <int> Observers = Graph->getObservers();

	int i = 0;
	for (i = 0; i < TimeStepsNumber; i++) {
		for (int j = 0; j < UpdatesNumber; j++) {
			//INFECTION!			
			int InfectingIndex = RandomInteger(VerticeNumber - 1);
			if (Graph->Structure[InfectingIndex].size() > 0 && EpidemyState[InfectingIndex] == 1) {
				for (unsigned int k = 0; k < Graph->Structure[InfectingIndex].size(); k++) {
					float InfectionTest = (float)rand() / RAND_MAX;
					if (InfectionTest >(1 - Infectiousness) && EpidemyState[Graph->Structure[InfectingIndex][k]] == 0) {
						EpidemyState[Graph->Structure[InfectingIndex][k]] = 1;
						InfectedNumber++;
						SusceptibleNumber--;

						if (std::find(Observers.begin(), Observers.end(), Graph->Structure[InfectingIndex][k]) != Observers.end()) {
							int pos = std::find(Observers.begin(), Observers.end(), Graph->Structure[InfectingIndex][k]) - Observers.begin();
							ObserversStatusChanged[pos] = i;
						}
					}
				}
			}
		}
		getEpidemyStateInFile(*EpidemyFile);

		if (breakIfCuredQuickly) {
			if (i < 20 && InfectedNumber == 0) {
				simulationSucceeded = false;
				break;
			}
		}
	}

	fclose(EpidemyFile);

	if (i == TimeStepsNumber)
		simulationSucceeded = true;
}

void SI::simulate_epidemy_no_file(int TimeStepsNumber, int UpdatesNumber, bool breakIfCuredQuickly, bool &simulationSucceeded) {
	SusceptibleNumber = Graph->getVerticeNumber() - InfectedNumber;

	int VerticeNumber = Graph->getVerticeNumber();
	std::vector <int> Observers = Graph->getObservers();

	int i = 0;
	for (i = 0; i < TimeStepsNumber; i++) {
		for (int j = 0; j < UpdatesNumber; j++) {
			//INFECTION!			
			int InfectingIndex = RandomInteger(VerticeNumber - 1);
			if (Graph->Structure[InfectingIndex].size() > 0 && EpidemyState[InfectingIndex] == 1) {
				for (unsigned int k = 0; k < Graph->Structure[InfectingIndex].size(); k++) {
					float InfectionTest = (float)rand() / RAND_MAX;
					if (InfectionTest >(1 - Infectiousness) && EpidemyState[Graph->Structure[InfectingIndex][k]] == 0) {
						EpidemyState[Graph->Structure[InfectingIndex][k]] = 1;
						InfectedNumber++;
						SusceptibleNumber--;

						if (std::find(Observers.begin(), Observers.end(), Graph->Structure[InfectingIndex][k]) != Observers.end()) {
							int pos = std::find(Observers.begin(), Observers.end(), Graph->Structure[InfectingIndex][k]) - Observers.begin();
							ObserversStatusChanged[pos] = i;
						}
					}
				}
			}
		}

		if (breakIfCuredQuickly) {
			if (i < 20 && InfectedNumber == 0) {
				simulationSucceeded = false;
				break;
			}
		}
	}

	if (i == TimeStepsNumber)
		simulationSucceeded = true;
}