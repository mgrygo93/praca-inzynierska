#ifndef BA_H //Barabasi-Albert graph
#define BA_H

#include "graph.h"
#include <vector>
#include <string>

class BA : public graph
{
private:
	void addSingleEdge(int currentVertex, std::vector<int> &currentConnections);
	int m0;
	float minimumDegree;
public:
	BA();
	BA(int Number, int m0_, float minimumDegree_);
	BA(int Number, std::string filename);
	~BA();

	virtual void createGraphStructure(bool writeOperationTime);
	virtual void resetGraphStructure();	
};

#endif //BA