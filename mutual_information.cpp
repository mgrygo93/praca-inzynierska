#include <algorithm>
#include <iostream>
#include <fstream>
#include <time.h>
#include <string>
#include <array>
#include <vector>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "mutual_information.h"
#include "functions.h"

mutual_information::mutual_information() {

}

mutual_information::mutual_information(EpidemyModel *existing_model, int number_of_iterations_) {
	model = existing_model;
	VerticeNumber = model->getVerticeNumber();
	number_of_iterations = number_of_iterations_;
}

mutual_information::~mutual_information() {

}

void mutual_information::perform_data_gathering(bool write_status) {
	float operations_time_so_far = 0.0;
	clock_t t1, t2;
	std::vector <std::string> v1;
	std::vector <int> v2;

	for (int i = 0; i < VerticeNumber; i++) {
		if (write_status) {
			t1 = clock();
			write_estimated_time_left(i, VerticeNumber, operations_time_so_far);
		}

		for (int j = 0; j < number_of_iterations; j++) {
			performSimulationForRemoved_no_file(model, i, 50, VerticeNumber);
			std::string tmp = model->getObserversStatusChanged_desiredFormat();

			if (find(v1.begin(), v1.end(), tmp) != v1.end()){
				int pos = std::find(v1.begin(), v1.end(), tmp) - v1.begin();
				v2[pos]++;
			}
			else {
				v1.push_back(tmp);
				v2.push_back(1);
			}			
		}

		data1.push_back(v1);
		data2.push_back(v2);
		v1.clear();
		v2.clear();

		if (write_status) {
			t2 = clock();
			operations_time_so_far += (float)(t2 - t1) / 1000;
		}
	}
}

void mutual_information::save_chosen_index_data(std::string filename, size_t index) {
	remove(filename.c_str());
	FILE *file = fopen(filename.c_str(), "w");

	if (index < 0 || index > data1[index].size() - 1) {
		printf("Wrong index!");
	} else {
		for (size_t i = 0; i < data1[index].size(); i++) {
			fprintf(file, "%s %d\n", data1[index][i].c_str(), data2[index][i]);
		}
	}

	fclose(file);
}

void mutual_information::write_unique_sequences_number(int index) {
	int unique_sequences_number = 0;
	for (size_t i = 0; i < data2[index].size(); i++) {
		if (data2[index][i] == 1)
			unique_sequences_number++;
	}
	printf("\ntmp = %d\n", unique_sequences_number);
}

void mutual_information::create_vector_of_total_sequences(bool write_status) {
	std::array <int, 2> index_pair = {0, 0};
	float operations_time_so_far = 0.0;
	clock_t t1, t2;
	if (write_status) {
		printf("\nCreating histogram!\n");
	}
	
	size_t data1_size = data1.size();
	for (size_t i = 0; i < data1_size; i++) {
		if (write_status) {
			t1 = clock();
			write_estimated_time_left(i, data1_size, operations_time_so_far);
		}

		for (size_t j = 0; j < data1[i].size(); j++) {
			if (find(sequences.begin(), sequences.end(), data1[i][j]) == sequences.end()) {
				sequences.push_back(data1[i][j]);
				index_pair[0] = i;
				index_pair[1] = j;
				sequences_indices.push_back(index_pair);
			}
		}

		if (write_status) {
			t2 = clock();
			operations_time_so_far += (float)(t2 - t1) / 1000;
		}
	}
}

void mutual_information::write_sequences_in_file(std::string filename) {
	remove(filename.c_str());
	FILE *file = fopen(filename.c_str(), "w");

	for (size_t i = 0; i < sequences.size(); i++) {
		fprintf(file, "%s\n", sequences[i].c_str());
	}

	fclose(file);
}

void mutual_information::write_sequences_with_indices_in_file(std::string filename) {
	remove(filename.c_str());
	FILE *file = fopen(filename.c_str(), "w");

	for (size_t i = 0; i < sequences.size(); i++) {
		fprintf(file, "%s %d %d\n", sequences[i].c_str(), sequences_indices[i][0], sequences_indices[i][1]);
	}

	fclose(file);
}

int mutual_information::get_sequences_number_with_repetitions() {
	int sequences_number = 0;

	for (size_t i = 0; i < data1.size(); i++) {
		sequences_number += data1[i].size();
	}

	return sequences_number;
}

void mutual_information::create_histogram(bool sequences_vector_created, bool write_status) {
	if (!sequences_vector_created) {
		create_vector_of_total_sequences(write_status);
	}

	float operations_time_so_far = 0.0;
	clock_t t1, t2;
	if (write_status) {
		printf("\nCreating histogram!\n");
	}

	std::vector <int> sequences_occurences_single_index;
	for (int i = 0; i < VerticeNumber; i++) {
		if (write_status) {
			t1 = clock();
			write_estimated_time_left(i, VerticeNumber, operations_time_so_far);
		}

		for (size_t j = 0; j < sequences.size(); j++) {
			if (find(data1[i].begin(), data1[i].end(), sequences[j]) != data1[i].end()) {
				int pos = std::find(data1[i].begin(), data1[i].end(), sequences[j]) - data1[i].begin();
				sequences_occurences_single_index.push_back(data2[i][pos]);
			} else {
				sequences_occurences_single_index.push_back(0);
			}
		}

		sequence_occurences.push_back(sequences_occurences_single_index);
		sequences_occurences_single_index.clear();

		if (write_status) {
			t2 = clock();
			operations_time_so_far += (float)(t2 - t1) / 1000;
		}
	}
}

void mutual_information::write_histogram_to_file(std::string filename) {
	remove(filename.c_str());
	FILE *file = fopen(filename.c_str(), "w");

	for (size_t i = 0; i < sequence_occurences.size(); i++) {		
		for (size_t j = 0; j < sequence_occurences[i].size(); j++) {
			fprintf(file, "%d ", sequence_occurences[i][j]);
		}
		fprintf(file, "\n");
	}

	fclose(file);
}

//for empty 'sequence_occurences'
void mutual_information::read_histogram_from_file(std::string filename) {
	printf("Reading histogram from file...\n");
	unsigned int number_of_sequences = get_number_of_chars_in_line(filename);
	unsigned int i = 0;
	unsigned int j = 0;
	int current_index = 0;

	std::vector <int> single_vertex;
	std::fstream file(filename, std::ios_base::in);

	while (file >> current_index) {
		single_vertex.push_back(current_index);
		i++;

		if (i == number_of_sequences) {
			sequence_occurences.push_back(single_vertex);
			single_vertex.clear();
			i = 0;
		}
	}
}

//additional calculation function
int mutual_information::get_sum_of_row(int row_index) {
	int sum_of_row = 0;

	for (size_t i = 0; i < sequence_occurences[row_index].size(); i++) {
		sum_of_row += sequence_occurences[row_index][i];
	}

	return sum_of_row;
}

//additional calculation function
int mutual_information::get_sum_of_all() {
	int sum_of_all = 0;

	for (size_t i = 0; i < sequence_occurences.size(); i++) {
		sum_of_all += get_sum_of_row(i);
	}

	return sum_of_all;
}

//wartosc prawdopodobienstwa brzegowego
double mutual_information::p_y_(int column_index, int sum_of_all) {
	double p_y = 0.0;

	for (size_t i = 0; i < sequence_occurences.size(); i++) {
		p_y += sequence_occurences[i][column_index];
	}

	return p_y / (double)sum_of_all;
}

void mutual_information::calculate_MI(bool write_status) {
	float operations_time_so_far = 0.0;
	clock_t t1, t2;
	int sum_of_all = get_sum_of_all();
	MI = 0.0;
	Entropy_xy = 0.0;
	Entropy_x = 0.0;
	Entropy_y = 0.0;

	double p_x, p_y, p_xy;
	p_x = 1.0 / (double)VerticeNumber;
	Entropy_x = - VerticeNumber * p_x * log2(p_x);

	for (size_t i = 0; i < sequence_occurences.size(); i++)	{
		if (write_status) {
			t1 = clock();
			write_estimated_time_left(i, sequence_occurences.size(), operations_time_so_far);
		}

		for (size_t j = 0; j < sequence_occurences[i].size(); j++) {
			if (sequence_occurences[i][j] != 0) {
				p_y = p_y_(j, sum_of_all);
				p_xy = (double)sequence_occurences[i][j] / (double)sum_of_all;
				MI += p_xy * log2(p_xy / (p_x * p_y));
				Entropy_xy += - p_xy * log2(p_xy);
				
				if (i == 0) {
					Entropy_y += -p_y * log2(p_y);
				}
			}
		}

		if (write_status) {
			t2 = clock();
			operations_time_so_far += (float)(t2 - t1) / 1000;
		}
	}

	printf("\n");
}

double mutual_information::get_MI() {
	return MI;
}

std::array <double, 3> mutual_information::get_Entropy() {
	std::array <double, 3> Entropy = { Entropy_xy, Entropy_x, Entropy_y };
	return Entropy;
}