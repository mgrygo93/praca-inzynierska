#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <algorithm>
#include <vector>
#include <time.h>
#include "ER.h"
#include "BA.h"
#include "SI.h"
#include "SIS.h"
#include "SIR.h"
#include "custom_SIR.h"
#include "functions.h"
#include "mutual_information.h"

using namespace std;

int main() {
	srand( (unsigned int) time(NULL) );

	int choice1 = 0, choice2 = 0, choice3 = 0;
	printf("1. Pojedyczna symulacja\n2. Obliczanie informacji wzajemnej\n");	
	printf("Wybierz tryb pracy podajac numer obok opcji\n");
	cin >> choice1;

	switch (choice1) {
	case 1:
	{
		int VerticeNumber_inner;
		float k = 6.0f, m = 3.0f;

		printf("Podaj liczbe wierzcholkow grafu\n");
		cin >> VerticeNumber_inner;

		printf("1. Graf ER\n2. Graf BA\n");
		printf("Wybierz typ grafu\n");
		cin >> choice2;		

		switch (choice2) {
		case 1:
		{			
			printf("Podaj sredni stopien wierzholkow grafu\n");
			cin >> k;			
			break;
		}
		case 2:
		{
			printf("Podaj minimalny stopien wierzholkow grafu\n");
			cin >> m;			
			break;
		}
		default:
		{
			printf("Nieprawidlowa wartosc!\n");
			break;
		}
		}

		printf("1. SI\n2. SIS\n3. SIR\n4. Model plotek\n");
		printf("Wybierz model rozchodzenia sie zaburzenia\n");
		cin >> choice3;

		ER ER_Graph(VerticeNumber_inner, k);
		BA BA_Graph(VerticeNumber_inner, 3, m);

		float infectionRate = 0.4f, recoveryRate = 0.2f, removeRate = 0.2f;
		string filename = "output.txt";

		printf("Podaj nazwe pliku, do ktorego beda zapisywane wyniki\n");
		cin >> filename;
		printf("Podaj prawdopodobienstwo zakazenia (od 0 do 1)\n");
		cin >> infectionRate;

		switch (choice3) {
		case 1:
		{
			typedef EpidemyModel *Models;
			Models ResearchedModels[1];
			if (choice2 == 1) {
				ResearchedModels[0] = new SI(ER_Graph, infectionRate);
			}
			else {
				ResearchedModels[0] = new SI(BA_Graph, infectionRate);
			}
			bool tmp;
			ResearchedModels[0]->infectFirstWithNeighbours();
			ResearchedModels[0]->simulateEpidemy(300, VerticeNumber_inner, filename, false, tmp);
			break;
		}
		case 2:
		{
			printf("Podaj prawdopodobienstwo wyzdrowienia (od 0 do 1)\n");
			cin >> recoveryRate;

			typedef EpidemyModel *Models;
			Models ResearchedModels[1];
			if (choice2 == 1) {
				ResearchedModels[0] = new SIS(ER_Graph, infectionRate, recoveryRate);
			}
			else {
				ResearchedModels[0] = new SIS(BA_Graph, infectionRate, recoveryRate);
			}
			bool tmp;
			ResearchedModels[0]->infectFirstWithNeighbours();
			ResearchedModels[0]->simulateEpidemy(300, VerticeNumber_inner, filename, false, tmp);
			break;
		}
		case 3:
		{
			printf("Podaj prawdopodobienstwo uodpornienia (od 0 do 1)\n");
			cin >> removeRate;

			typedef EpidemyModel *Models;
			Models ResearchedModels[1];
			if (choice2 == 1) {
				ResearchedModels[0] = new SIR(ER_Graph, infectionRate, removeRate);
			}
			else {
				ResearchedModels[0] = new SIR(BA_Graph, infectionRate, removeRate);
			}
			bool tmp;
			ResearchedModels[0]->infectFirstWithNeighbours();
			ResearchedModels[0]->simulateEpidemy(300, VerticeNumber_inner, filename, false, tmp);
			break;
		}
		case 4:
		{
			printf("Podaj prawdopodobienstwo uodpornienia (od 0 do 1)\n");
			cin >> removeRate;

			typedef EpidemyModel *Models;
			Models ResearchedModels[1];
			if (choice2 == 1) {
				ResearchedModels[0] = new custom_SIR(ER_Graph, infectionRate, removeRate);
			}
			else {
				ResearchedModels[0] = new custom_SIR(BA_Graph, infectionRate, removeRate);
			}
			bool tmp;
			ResearchedModels[0]->infectFirstWithNeighbours();
			ResearchedModels[0]->simulateEpidemy(300, VerticeNumber_inner, filename, false, tmp);
			break;
		}
		default:
		{
			printf("Nieprawidlowa wartosc!\n");
			break;
		}
		}

		break;
	}
	case 2:
	{
		int VerticeNumber_inner = 100;
		float k = 6.0f, m = 3.0f;

		printf("Podaj liczbe wierzcholkow grafu\n");
		cin >> VerticeNumber_inner;

		printf("Podaj sredni stopien wierzholkow grafu ER\n");
		cin >> k;
		
		printf("Podaj minimalny stopien wierzholkow grafu\n");
		cin >> m;

		ER ER_Graph(VerticeNumber_inner, k);
		BA BA_Graph(VerticeNumber_inner, 3, m);

		int observersNumber = 4;
		printf("Podaj ilosc obserwatorow\n");
		cin >> observersNumber;
		
		ER_Graph.addObservers_randomly(observersNumber);
		BA_Graph.addObservers_randomly(observersNumber);

		printf("1. SI\n2. SIS\n3. SIR\n4. Model plotek\n");
		printf("Wybierz model rozchodzenia sie zaburzenia\n");
		cin >> choice3;		

		float infectionRate = 0.4f, recoveryRate = 0.2f, removeRate = 0.2f;
		int MI_datapoints_number = 1;
		const int number_of_iterations = 5000;

		printf("Podaj ilosc wartosci informacji wzajemnej\n");
		cin >> MI_datapoints_number;
		printf("Podaj prawdopodobienstwo zakazenia (od 0 do 1)\n");
		cin >> infectionRate;

		switch (choice3) {
		case 1:
		{
			typedef EpidemyModel *Models;
			Models ResearchedModels[2];
			ResearchedModels[0] = new SI(ER_Graph, infectionRate);
			ResearchedModels[1] = new SI(BA_Graph, infectionRate);

			MI_ER_BA(ResearchedModels[0], ResearchedModels[1], MI_datapoints_number, "SI", number_of_iterations);

			ResearchedModels[0]->~EpidemyModel();
			ResearchedModels[1]->~EpidemyModel();
			break;
		}
		case 2:
		{
			printf("Podaj prawdopodobienstwo wyzdrowienia (od 0 do 1)\n");
			cin >> recoveryRate;

			typedef EpidemyModel *Models;
			Models ResearchedModels[2];
			ResearchedModels[0] = new SIS(ER_Graph, infectionRate, recoveryRate);
			ResearchedModels[1] = new SIS(BA_Graph, infectionRate, recoveryRate);

			MI_ER_BA(ResearchedModels[0], ResearchedModels[1], MI_datapoints_number, "SIS", number_of_iterations);

			ResearchedModels[0]->~EpidemyModel();
			ResearchedModels[1]->~EpidemyModel();
			break;
		}
		case 3:
		{
			printf("Podaj prawdopodobienstwo uodpornienia (od 0 do 1)\n");
			cin >> removeRate;

			typedef EpidemyModel *Models;
			Models ResearchedModels[2];
			ResearchedModels[0] = new SIR(ER_Graph, infectionRate, removeRate);
			ResearchedModels[1] = new SIR(BA_Graph, infectionRate, removeRate);

			MI_ER_BA(ResearchedModels[0], ResearchedModels[1], MI_datapoints_number, "SIR", number_of_iterations);

			ResearchedModels[0]->~EpidemyModel();
			ResearchedModels[1]->~EpidemyModel();
			break;
		}
		case 4:
		{
			printf("Podaj prawdopodobienstwo uodpornienia (od 0 do 1)\n");
			cin >> removeRate;

			typedef EpidemyModel *Models;
			Models ResearchedModels[2];
			ResearchedModels[0] = new custom_SIR(ER_Graph, infectionRate, removeRate);
			ResearchedModels[1] = new custom_SIR(BA_Graph, infectionRate, removeRate);

			MI_ER_BA(ResearchedModels[0], ResearchedModels[1], MI_datapoints_number, "Model_plotek", number_of_iterations);

			ResearchedModels[0]->~EpidemyModel();
			ResearchedModels[1]->~EpidemyModel();
			break;
		}
		default:
		{
			printf("Nieprawidlowa wartosc!\n");
			break;
		}
		}

		break;
	}
	default: 
	{
		printf("Nieprawidlowa wartosc!\n");
		break;
	}
	}	

	return 0;
}