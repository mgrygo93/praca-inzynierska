#ifndef MUTUAL_INFORMATION_H
#define MUTUAL_INFORMATION_H

#include <string>
#include <array>
#include <vector>
#include "EpidemyModel.h"

class mutual_information {
private:
	EpidemyModel *model;

	std::vector <std::vector <std::string>> data1;
	std::vector <std::vector <int>> data2;

	int VerticeNumber;
	int number_of_iterations; //5000 is good

	std::vector <std::string> sequences;
	std::vector <std::array <int, 2>> sequences_indices;

	std::vector <std::vector <int>> sequence_occurences; //histogram
	double MI;
	double Entropy_xy;
	double Entropy_x;
	double Entropy_y;
public:
	mutual_information();
	mutual_information(EpidemyModel *existing_model, int number_of_iterations_);
	~mutual_information();

	void perform_data_gathering(bool write_status);
	void save_chosen_index_data(std::string filename, size_t index);
	void write_unique_sequences_number(int index);
	void create_vector_of_total_sequences(bool write_status);
	void write_sequences_in_file(std::string filename);
	void write_sequences_with_indices_in_file(std::string filename);
	int get_sequences_number_with_repetitions();
	void create_histogram(bool sequences_vector_created, bool write_status);
	void write_histogram_to_file(std::string filename);
	void read_histogram_from_file(std::string filename);

	int get_sum_of_row(int row_index);
	int get_sum_of_all();
	double p_y_(int column_index, int sum_of_all);
	void calculate_MI(bool write_status);
	double get_MI();
	std::array <double, 3> get_Entropy();
};

#endif //MUTUAL_INFORMATION_H