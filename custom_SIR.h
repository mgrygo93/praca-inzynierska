#ifndef CUSTOM_SIR_H
#define CUSTOM_SIR_H

#include "EpidemyModel.h"
#include "graph.h"

class custom_SIR : public EpidemyModel
{
private:
	float RemoveRate;	
public:
	custom_SIR();
	custom_SIR(graph &ExistingGraph, float InfectRate, float RemovingRate);
	~custom_SIR();

	virtual void getEpidemyStateInFile(FILE &file);

	virtual void resetEpidemyStates();
	int getRemovedNumber();
	virtual void simulateEpidemy(int TimeStepsNumber, int UpdatesNumber, std::string fileName, bool breakIfCuredQuickly, bool &simulationSucceeded);	
	virtual void simulate_epidemy_no_file(int TimeStepsNumber, int UpdatesNumber, bool breakIfCuredQuickly, bool &simulationSucceeded);
};

#endif //CUSTOM_SIR