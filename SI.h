#ifndef SI_H
#define SI_H

#include "EpidemyModel.h"
#include "graph.h"

class SI : public EpidemyModel
{
public:
	SI();
	SI(graph &ExistingGraph, float InfectRate);
	~SI();

	virtual void getEpidemyStateInFile(FILE &file);

	virtual void resetEpidemyStates();
	virtual void simulateEpidemy(int TimeStepsNumber, int UpdatesNumber, std::string fileName, bool breakIfCuredQuickly, bool &simulationSucceeded);
	virtual void simulate_epidemy_no_file(int TimeStepsNumber, int UpdatesNumber, bool breakIfCuredQuickly, bool &simulationSucceeded);
};

#endif //SI