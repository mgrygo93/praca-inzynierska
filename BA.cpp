#include <time.h>
#include <iostream>
#include <array>
#include <vector>
#include "functions.h"
#include "BA.h"

BA::BA() {
}

//minimumDegree should be from <1, m0>, if 1 then Tree else regular BA graph
BA::BA(int Number, int m0_, float minimumDegree_) {
	VerticeNumber = Number;
	m0 = m0_;
	minimumDegree = minimumDegree_;

	createGraphStructure(false);
}

BA::BA(int Number, std::string filename) {
	VerticeNumber = Number;

	clock_t t1, t2;
	t1 = clock();

	getEdgesFromFile(filename);
	Structure = new std::vector<int>[VerticeNumber];
	createStructureFromEdges();

	t2 = clock();

	float OperationsTime = (float)(t2 - t1) / 1000;
	printf("Loading ER graph from file took: %f seconds!\n", OperationsTime);
}


BA::~BA() {
	delete[] Structure;
}

void BA::addSingleEdge(int currentVertex, std::vector<int> &currentConnections) {
	int verticeToConnectIndex = RandomInteger(currentConnections.size() - 1);
	int verticeToConnect = currentConnections[verticeToConnectIndex];

	while (verticeToConnect == currentVertex) {
		verticeToConnectIndex = RandomInteger(currentConnections.size() - 1);
		verticeToConnect = currentConnections[verticeToConnectIndex];
	}

	Structure[currentVertex].push_back(verticeToConnect);
	Structure[verticeToConnect].push_back(currentVertex);

	currentConnections.push_back(verticeToConnect);
	currentConnections.push_back(currentVertex);
}

void BA::createGraphStructure(bool writeOperationTime) {
	clock_t t1, t2;
	if (writeOperationTime) {		
		t1 = clock();
	}

	Structure = new std::vector<int>[VerticeNumber];
	std::vector<int> currentConnections;

	//creating fully connected graph with m0 vertices
	for (int i = 0; i < m0; ++i) {
		for (int j = 0; j < m0; ++j) {
			if (i != j) {
				Structure[i].push_back(j);
				currentConnections.push_back(j);
			}
		}
	}

	double fractpart, intpart;
	fractpart = modf(minimumDegree, &intpart);

	for (int i = m0; i < VerticeNumber; i++) {
		for (int j = 0; j < (int)intpart; j++) {
			addSingleEdge(i, currentConnections);
		}
		if (RandomFloat() < fractpart) {
			addSingleEdge(i, currentConnections);
		}
	}

	for (unsigned int i = 0; i < currentConnections.size(); i++) {
		int FirstIndex = currentConnections[i];
		i++;
		int SecondIndex = currentConnections[i];

		std::array<int, 2> Edge = { FirstIndex, SecondIndex };
		Edges.push_back(Edge);
	}

	if (writeOperationTime) {
		t2 = clock();
		float OperationsTime = (float)(t2 - t1) / 1000;
		printf("Creating BA graph took: %f seconds!\n", OperationsTime);
	}
}

void BA::resetGraphStructure() {
	delete[] Structure;
	Edges.clear();
	createGraphStructure(false);
}