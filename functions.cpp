#include <stdlib.h>
#include <string>
#include <vector>
#include <fstream>
#include "functions.h"
#include "EpidemyModel.h"
#include "mutual_information.h"

//returns integer from 0 to MaxValue
int RandomInteger(int MaxValue) {
	return rand() % (MaxValue + 1);
}

//returns float from 0 to 1
float RandomFloat() {
	return (float)rand() / RAND_MAX;
}

bool Equals(int* Array1, int* Array2, int ElementsNumber) {
	bool isEqual = true;
	for (int i = 0; i < ElementsNumber; i++) {
		if (Array1[i] != Array2[i]) {
			isEqual = false;
			break;
		}
	}
	return isEqual;
}

void performSingleSimulation(EpidemyModel* epidemyModel, int numberOfNeighbours, int which, std::string fileName, int timeStepNumber, int updatesNumber) {
	if (epidemyModel->infectWithNumberOfNeighbours(numberOfNeighbours, which)) {
		bool simulationSucceeded = false;
		while (!simulationSucceeded) {
			epidemyModel->resetEpidemyStates();
			epidemyModel->infectWithNumberOfNeighbours(numberOfNeighbours, which);
			epidemyModel->simulateEpidemy(timeStepNumber, updatesNumber, fileName, true, simulationSucceeded);
		}		
	}
}

void performSimulationForRemoved(EpidemyModel* epidemyModel, int index, std::string fileName, int timeStepNumber, int updatesNumber) {
	bool simulationSucceeded = false;
	int reset_number = 0;
	while (!simulationSucceeded && reset_number < 100) {
		epidemyModel->resetEpidemyStates();
		epidemyModel->infectWithIndex(index);
		epidemyModel->simulateEpidemy(timeStepNumber, updatesNumber, fileName, true, simulationSucceeded);
		reset_number++;
	}
}

void performSimulationForRemoved_no_file(EpidemyModel* epidemyModel, int index, int timeStepNumber, int updatesNumber) {
	bool simulationSucceeded = false;
	int reset_number = 0;
	while (!simulationSucceeded && reset_number < 100) {
		epidemyModel->resetEpidemyStates();
		epidemyModel->infectWithIndex(index);
		epidemyModel->simulate_epidemy_no_file(timeStepNumber, updatesNumber, true, simulationSucceeded);
		reset_number++;
	}
}

//pobieranie R od indexu i stopnia wierzcholka
void performSimulationForEveryNodeForRemoved(std::string fileName, EpidemyModel* epidemyModel, int timeStepNumber, int updatesNumber) {
	remove(fileName.c_str());

	FILE* fp;
	fp = fopen(fileName.c_str(), "a");

	//k - node degree, R - number of recoverd
	fprintf(fp, "index k R\n");

	int VerticeNumber = epidemyModel->getVerticeNumber();
	for (int i = 0; i < VerticeNumber; i++) {
		epidemyModel->resetEpidemyStates();
		performSimulationForRemoved(epidemyModel, i, "trash_tmp", timeStepNumber, updatesNumber);
		fprintf(fp, "%d %d %d\n", i, epidemyModel->getChosenIndexDegree(i), epidemyModel->getRemovedNumber());
		printf("Finished %d/%d iteration!\n", i, VerticeNumber);
	}

	fclose(fp);
}

void saveDataForMutualInformation(std::string filename, std::vector<std::string> v1, std::vector<int> v2) {
	remove(filename.c_str());
	FILE *file = fopen(filename.c_str(), "w");

	for (unsigned int i = 0; i < v1.size(); i++) {
		fprintf(file, "%s %d\n", v1[i].c_str(), v2[i]);
	}

	fclose(file);
}

void write_estimated_time_left(int current_timestep_number, int number_of_iterations, float operations_time_so_far) {
	if (current_timestep_number == 0) {
		printf("\r                                                                 ");
		printf("\r%d/%d timestep in progress!", current_timestep_number + 1, number_of_iterations);
	}
	else {
		float estimatedTimeLeft = operations_time_so_far / current_timestep_number * (number_of_iterations - current_timestep_number - 1);
		if (estimatedTimeLeft < 60) {
			printf("\r                                                                 ");
			printf("\r%d/%d timestep! Time to finish: %f[s]", current_timestep_number + 1, number_of_iterations, estimatedTimeLeft);
		} else if (estimatedTimeLeft > 60 && estimatedTimeLeft < 3600) {
			double seconds, minutes;
			seconds = modf(estimatedTimeLeft / 60, &minutes);
			seconds *= 60;
			printf("\r                                                                 ");
			printf("\r%d/%d timestep! Time to finish: %d[min] %d[s]", current_timestep_number + 1, number_of_iterations, (int)minutes, (int)seconds);
		} else {
			double hours, seconds, minutes;
			seconds = modf(estimatedTimeLeft / 60, &minutes);			
			seconds *= 60;
			minutes = modf(minutes / 60, &hours);
			minutes *= 60;
			printf("\r                                                                 ");
			printf("\r%d/%d timestep! Time to finish: %d[h] %d[min] %d[s]", current_timestep_number + 1, number_of_iterations, (int)hours, (int)minutes, (int)seconds);
		}
	}
}

//assuming there's space after every character
int get_number_of_chars_in_line(std::string filename) {
	std::ifstream file(filename);
	std::string line;

	int number_of_chars = 0;
	getline(file, line, '\n');

	for (unsigned int i = 0; i < line.length(); i++) {
		if (line.at(i) == ' ') {
			number_of_chars++;
		}
	}
	file.close();

	return number_of_chars;
}

void calculate_mutual_information_in_file(EpidemyModel* epidemyModel, std::string filename, int number_of_iterations, int number_of_data_points, bool write_status, std::string histogram_filename) {
	remove(filename.c_str());

	FILE* fp;
	fp = fopen(filename.c_str(), "a");
	fprintf(fp, "MI Entropy_xy Entropy_x Entropy_y\n");

	for (int i = 0; i < number_of_data_points; i++) {
		printf("%d/%d data point for %s\n", i + 1, number_of_data_points, filename.c_str());
				
		std::string histogram_name = histogram_filename + std::to_string(i) + ".txt";

		mutual_information *MI = new mutual_information(epidemyModel, number_of_iterations);
		MI->perform_data_gathering(true);
		MI->create_vector_of_total_sequences(false);
		MI->create_histogram(true, false);
		MI->calculate_MI(false);
		fprintf(fp, "%f %f %f %f\n", MI->get_MI(), MI->get_Entropy()[0], MI->get_Entropy()[1], MI->get_Entropy()[2]);
		MI->write_histogram_to_file(histogram_name);
		MI->~mutual_information();

		if (i != (number_of_data_points - 1)) {
			epidemyModel->resetGraphStructure();
		}
	}

	fclose(fp);
}

//doesn't save histogram in file
void calculate_mutual_information_in_file_same_topology_for_models(EpidemyModel* epidemyModel_1, std::string filename_1, 
	EpidemyModel* epidemyModel_2, std::string filename_2, int number_of_iterations, int number_of_data_points, bool write_status) {
	remove(filename_1.c_str());
	remove(filename_2.c_str());

	FILE *fp1, *fp2;
	fp1 = fopen(filename_1.c_str(), "a");
	fprintf(fp1, "MI Entropy_xy Entropy_x Entropy_y\n");
	fp2 = fopen(filename_2.c_str(), "a");
	fprintf(fp2, "MI Entropy_xy Entropy_x Entropy_y\n");

	for (int i = 0; i < number_of_data_points; i++) {
		printf("%d/%d data point for %s\n", i + 1, number_of_data_points, filename_1.c_str());

		mutual_information *MI_1 = new mutual_information(epidemyModel_1, number_of_iterations);
		MI_1->perform_data_gathering(true);
		MI_1->create_vector_of_total_sequences(false);
		MI_1->create_histogram(true, false);
		MI_1->calculate_MI(false);
		fprintf(fp1, "%f %f %f %f\n", MI_1->get_MI(), MI_1->get_Entropy()[0], MI_1->get_Entropy()[1], MI_1->get_Entropy()[2]);
		MI_1->~mutual_information();

		printf("%d/%d data point for %s\n", i + 1, number_of_data_points, filename_2.c_str());

		mutual_information *MI_2 = new mutual_information(epidemyModel_2, number_of_iterations);
		MI_2->perform_data_gathering(true);
		MI_2->create_vector_of_total_sequences(false);
		MI_2->create_histogram(true, false);
		MI_2->calculate_MI(false);
		fprintf(fp2, "%f %f %f %f\n", MI_2->get_MI(), MI_2->get_Entropy()[0], MI_2->get_Entropy()[1], MI_2->get_Entropy()[2]);
		MI_2->~mutual_information();

		if (i != (number_of_data_points - 1)) {
			epidemyModel_1->resetGraphStructure();
		}
	}

	fclose(fp1);
	fclose(fp2);
}

void MI_ER_BA(EpidemyModel* epidemyModel_1, EpidemyModel* epidemyModel_2, int MI_datapoints_number, std::string epidemy_model, int number_of_iterations) {
	std::string histogram_name_ER = epidemy_model + "_histogram_ER_";
	std::string histogram_name_BA = epidemy_model + "_histogram_BA_";		
	
	std::string ER_filename = epidemy_model + "_ER_MI.txt";
	std::string BA_filename = epidemy_model + "_BA_MI.txt";

	calculate_mutual_information_in_file(epidemyModel_1, ER_filename, number_of_iterations, MI_datapoints_number, true, histogram_name_ER);
	calculate_mutual_information_in_file(epidemyModel_2, BA_filename, number_of_iterations, MI_datapoints_number, true, histogram_name_BA);
}